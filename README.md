#JRIP, Routing Information Protocol Simulation

##Computer Networks Project 

The goal is to simulate a Routing Information Protocol (RIP) . This project is called JRIP because we use JSON for transmitting data between servers(which act as routers). The project is built in phases (jrip 1 through 4), each with some added functionality. Please view the project instructions for details on each one of the phases. RIP uses the Bellman-Ford algorithm to update routing data at each router. In this project, I built a program for a server; multiple instances of it can be run to create a network topology. Each of the routers uses the previously mentioned algorithm to find the shortest path to every other router in the topology. Messages can be transmitted between any two routers in the topology (using the jptraceroute program in the jrip4 folder.

##RESULTS.pdf contains sample commands and the topologies they produce for jrip steps 1 through 4.)