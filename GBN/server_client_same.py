from socket import *
import sys, os, math, time, json, copy, _thread
from random import random
server_dict = {}
def compute_whether_to_send():
	r = random()
	if r <= loss_probability:
		return False
	return True

def receiver():
	global server_dict
	rip_table = {
		"SEQ" : 1,
		"ACK" : 0,
		"Data" :
			{
				"Type" : "JRIP",
				"RIPTable": []
			}
		}

	reply_packet = {
		"SEQ" : 1,
		"ACK" : 0,
		}

	#takes the port number as command line arguments and create server socket
	serverIP="localhost"
	#the server porot is the first command line argument
	serverPort= int(sys.argv[2])

	#create the socket
	serverSocket=socket(AF_INET,SOCK_DGRAM)
	serverSocket.bind((serverIP,serverPort))
	#serverSocket.settimeout(3)
	print ("Listening on %s:%s"%(serverIP,serverPort))

	#initializes packet variables
	expectedseqnum=0
	ACK=0
	#RECEIVES DATA
	while True:
		packet,clientAddress= serverSocket.recvfrom(4096)
		received_json = json.loads(packet.decode())

		complete_address = "%s:%s"%(clientAddress[0],str(clientAddress[1]))

		if expectedseqnum == 0:
			server_dict[complete_address] = expectedseqnum
			expectedseqnum = -1

		#check value of expected seq number against seq number received - IN ORDER
		if received_json["SEQ"] == server_dict[complete_address]:
			print ("Received inorder, seq numb: %s"%server_dict[complete_address])
			#create ACK (seqnum,checksum)
			reply_packet["SEQ"] = -1
			reply_packet["ACK"] = server_dict[complete_address]

			print("sending ACK: %s"%server_dict[complete_address])

			server_dict[complete_address] = server_dict[complete_address] + 1
			#dictionary to JSON
			json_reply = json.dumps(reply_packet)
			serverSocket.sendto(json_reply.encode('utf-8'),(clientAddress[0], clientAddress[1]))

			print ("New Ack %s"%server_dict[complete_address])

		else:
			#default? discard packet and resend ACK for most recently received inorder pkt
			print ("Received out of order, seq num: %s"%received_json["SEQ"])

			reply_packet["SEQ"] = -1
			reply_packet["ACK"] = server_dict[complete_address]
			#dictionary to JSON
			json_reply = json.dumps(reply_packet)
			serverSocket.sendto(json_reply.encode('utf-8'),(clientAddress[0], clientAddress[1]))
			print ("New Ack sent%s"%server_dict[complete_address])

def sender():
	rip_table = {
		"SEQ" : 0,
		"ACK" : 0,
		"Data" :
			{
				"Type" : "JRIP",
				"RIPTable": []
			}
		}
	loss_probability = 0.4
	#takes the port number as command line arguments
	serverName="localhost"
	serverPort=int(sys.argv[1])

	#create client socket
	clientSocket = socket(AF_INET,SOCK_DGRAM)
	clientSocket.settimeout(0.001)

	start = time.time()

	#initializes window variables (upper and lower window bounds, position of next seq number)
	base=0
	nextSeqnum=0
	windowSize=5
	window = []

	while True:#This might have to be while TRUE
		if(nextSeqnum<base+windowSize):
			sndpkt = copy.deepcopy(rip_table)
			sndpkt["SEQ"] = nextSeqnum
			sndpkt["DATA"] = {"Type" : "JRIP","RIPTable": []}

			time.sleep(2)

			#send packet
			json_reply = json.dumps(sndpkt)
			clientSocket.sendto(json_reply.encode('utf-8'),(serverName, serverPort))
			print ("Sent data %s" % nextSeqnum)

			if base == nextSeqnum:
				lastackreceived = time.time()



			#increment variable nextSeqnum
			nextSeqnum = nextSeqnum + 1
			#append packet to window
			window.append(sndpkt)
			x = []
			for i in window:
				x.append(i["SEQ"])
			print(x)
		#RECEIPT OF AN ACK
		try:
			packet,serverAddress = clientSocket.recvfrom(4096)
			received_json = json.loads(packet.decode())
			print ("Received ack for %s"%received_json["ACK"])

			#slide window and reset timer
			while received_json["ACK"]>base:
				lastackreceived = time.time()
				del window[0]
				base = base + 1

		#TIMEOUT
		except:
			if(time.time()-lastackreceived>0.5):
				print("TIMEOUT")
				for i in window:
					json_reply = json.dumps(i)
					clientSocket.sendto(json_reply.encode('utf-8'),(serverName, serverPort))


	print ("connection closed")
	clientSocket.close()

_thread.start_new_thread(sender,())
_thread.start_new_thread(receiver,())

while 1:
    pass
