#!/usr/bin/env python3
"""This is the same as jrip21, except for one fix: RETRANSMISSIONS DO NOT COUNT in goodput calculations"""
"""ACK LOSSES have been implemented for this model as well"""

#test commands:try1
#python jrip21.py -p 8000 -l 0 localhost:8001:10 localhost:8002:10
#python jrip21.py -p 8001 -l 0.4 localhost:8000:10
#python jrip21.py -p 8002 -l 0.3 localhost:8000:10

#try2
#python jrip21.py -p 8000 -l 0 localhost:8001:10 localhost:8002:10
#python jrip21.py -p 8001 -l 0.4 localhost:8000:10
#python jrip21.py -p 8002 -l 0.3 localhost:8000:10  localhost:8001:10

import sys, argparse, time, socket, re, json, _thread, os, copy
from random import random
import queue

SLEEP_TIME = 0
WINDOW_SIZE = 5
TIMEOUT_VALUE = SLEEP_TIME + 0.5
MAX_PACKETS_SENT = 100

address_translation = {}
goodput ={}
dictionary_classifier = dict()

receiver_queue = queue.Queue()

ip_address_regex = re.compile(r'[0-9]{1,3}[.]{1}[0-9]{1,3}[.]{1}[0-9]{1,3}[.]{1}[0-9]{1,3}')

parser = argparse.ArgumentParser(description='Process the port number and loss.')
parser.add_argument('-p', metavar='port number', type=int, nargs=1, help='the receive port number', required=True)
parser.add_argument('-l', metavar='loss', type= float, nargs=1, help='loss probability, number between 0 and 1', required=True)
parser.add_argument('destinations', metavar='destinations', type=str, nargs='+', help='unlimited list of destination hosts and port numbers')

#os.system()
#sequence_numbers = []
args = vars(parser.parse_args())
listening_port_number = args['p'][0] #port number specified in the p flag
loss_probability = args['l'][0]
destinations = args['destinations']


ips_and_port_numbers = tuple()

address_regex = re.compile(r'.+?(?=:)')
port_regex = re.compile(r'\:(.*)')
link_weight_regex = re.compile(r'\:(.*)')

rip_table = {
	"SEQ" : 1,
	"ACK" : 0,
	"Data" :
		{
			"Type" : "JRIP",
			"RIPTable": []
		}
	}

no_colon_error = "You have input addresses and port numbers in the wrong format \n Please enter address and port number in the form address:port"
def compute_whether_to_send():
	r = random()
	if r <= loss_probability:
		return False
	return True

def create_address_dictionary():
	global rip_table
	global sequence_numbers
	for i in range(len(destinations)):
		#sequence_numbers.append(0)
		address_result = address_regex.findall(destinations[i])
		address = address_result[0] #retrieve the address from the return array from the regex

		#os.system("jrip2 -p %s -l %s localhost:%s"%(address,0,int(listening_port_number)))

		port_result = port_regex.findall(destinations[i])
		link_weight = int(link_weight_regex.findall(port_result[0])[0])

		#Add all the neighbors to the RIP table
		rip_table["Data"]["RIPTable"].append({"Dest": address, "Next": address, "Cost": link_weight})

		ip_address_result = ip_address_regex.findall(address)

		if len(ip_address_result) == 0:
			ip_address = socket.gethostbyname(address)
		else:
			ip_address = address

		#dictionary of ip addresses for every adress provided
		address_translation[address] = ip_address
		goodput[destinations[i]] = set()

def sender(address):
	#takes the port number as command line arguments
	address_result = address_regex.findall(address)
	port_result = port_regex.findall(address)
	link_weight_result = link_weight_regex.findall(port_result[0])
	port_result = address_regex.findall(port_result[0])

	serverName = address_translation[address_result[0]]
	serverPort = int(port_result[0])

	#create client socket
	clientSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	clientSocket.settimeout(0.001)

	start = time.time()

	#initializes window variables (upper and lower window bounds, position of next seq number)
	base=0
	nextSeqnum=0
	windowSize=5
	window = []
	#first = True
	total_packets_sent = 0
	while True:
		if(nextSeqnum<base+windowSize):
			#create packet(seqnum,data,checksum)
			#start = time.time()
			sndpkt = copy.deepcopy(rip_table)
			sndpkt["SEQ"] = nextSeqnum
			sndpkt["DATA"] = {"Type" : "JRIP","RIPTable": []}

			time.sleep(SLEEP_TIME)
			#send packet only if compute_whether_to_send decideds not to lose it
			total_packets_sent += 1
			if compute_whether_to_send():
				if sndpkt["SEQ"] not in goodput[address]:
					goodput[address].add(sndpkt["SEQ"])

				json_reply = json.dumps(sndpkt)
				clientSocket.sendto(json_reply.encode('utf-8'),(serverName, serverPort))
				print ("Sent data %s" % nextSeqnum)
			if total_packets_sent == MAX_PACKETS_SENT:
				#print(goodput)
				calculate_goodput(address)

			if base == nextSeqnum:
				lastackreceived = time.time()

			#increment variable nextSeqnum
			nextSeqnum = nextSeqnum + 1
			#append packet to window
			window.append(sndpkt)

			x = []
			for i in window:
				x.append(i["SEQ"])
			print(x)
		#RECEIPT OF AN ACK
		try:
			packet,serverAddress = clientSocket.recvfrom(4096)

			received_json = json.loads(packet.decode())
			print ("Received ack for %s"%received_json["ACK"])

			#slide window and reset timer
			while received_json["ACK"]>base:
				lastackreceived = time.time()
				del window[0]
				base = base + 1

		#TIMEOUT
		except:
			if(time.time()-lastackreceived>TIMEOUT_VALUE):
				lastackreceived = time.time()
				#print("TIMEOUT")
				for i in window:
					print("resending packet %s "%i["SEQ"])
					json_reply = json.dumps(i)
					#if compute_whether_to_send():
					clientSocket.sendto(json_reply.encode('utf-8'),(serverName, serverPort))


	print ("connection closed")
	clientSocket.close()

def calculate_goodput(address):
	file = open("%s.txt"%listening_port_number,"a")
	file.write("Address at this router: %s\n"%listening_port_number)
	file.write("Address of this neighbor: %s\n"% address)
	file.write("%s packets sent\n"%MAX_PACKETS_SENT)
	file.write("Number of unique packets sent: %s\n"% len(goodput[address]))
	file.write("Goodput: %s\n"% (len(goodput[address])/MAX_PACKETS_SENT))
	file.close()

def listening_thread(serverSocket):
	while True:
		packet,clientAddress= serverSocket.recvfrom(4096)
		receiver_queue.put((packet,clientAddress))

def receiver_responses(serverSocket):
	reply_packet = {
		"SEQ" : -1,
		"ACK" : 0,
		}

	while True:
		packet_to_process = receiver_queue.get()
		received_json = json.loads(packet_to_process[0].decode())
		IP = packet_to_process[1][0]
		port = packet_to_process[1][1]

		identifier = '%s:%s'%(IP,port)

		if identifier not in dictionary_classifier:
			dictionary_classifier[identifier] = 0

		#dictionary_classifier[identifier] corresponds to expected sequence number
		if received_json["SEQ"] == dictionary_classifier[identifier]:
			#create ACK (seqnum,checksum)
			reply_packet["ACK"] = dictionary_classifier[identifier]

			print("sending ACK: %s"%dictionary_classifier[identifier])
			dictionary_classifier[identifier] += 1

			if compute_whether_to_send():
				json_reply = json.dumps(reply_packet)
				serverSocket.sendto(json_reply.encode('utf-8'),(IP, port))
		else:
			print ("Received out of order, seq num: %s"%received_json["SEQ"])
			reply_packet["ACK"] = dictionary_classifier[identifier]

			if compute_whether_to_send():
				json_reply = json.dumps(reply_packet)
				serverSocket.sendto(json_reply.encode('utf-8'),(IP, port))

def main():

	serverName="localhost"
	serverPort= int(listening_port_number)
	serverSocket=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	serverSocket.bind((serverName,serverPort))

	print ("Listening on %s:%s"%(serverName,serverPort))

	create_address_dictionary()

	for destination in destinations:
		_thread.start_new_thread(sender,(destination,))

	_thread.start_new_thread(listening_thread,(serverSocket, ))
	_thread.start_new_thread(receiver_responses,(serverSocket, ))
	while 1:
	   pass

if __name__ == "__main__":
	main()
