#`python jtraceroute.py -p 8001 localhost:8002 localhost:8003`
import sys, argparse, time, socket, re, json, _thread, os, copy
address_translation = {}

ip_address_regex = re.compile(r'[0-9]{1,3}[.]{1}[0-9]{1,3}[.]{1}[0-9]{1,3}[.]{1}[0-9]{1,3}')
parser = argparse.ArgumentParser(description='Process the port number and loss.')
parser.add_argument('-p', metavar='port number', type=int, nargs=1, help='the receive port number', required=True)
parser.add_argument('destinations', metavar='destinations', type=str, nargs='+', help='unlimited list of destination hosts and port numbers')

args = vars(parser.parse_args())
listening_port_number = args['p'][0] #port number specified in the p flag\
destinations = args['destinations']

ips_and_port_numbers = tuple()

address_regex = re.compile(r'.+?(?=:)')
port_regex = re.compile(r'\:(.*)')
link_weight_regex = re.compile(r'\:(.*)')

traceroute = {
	"uni" : "aft2120",
	"SEQ" : 1,
	"ACK" : 0,
	"Data" :
	{
	"Type" : "TRACE",
	"Destination" : "",
	"Origin":"",
	"TRACE": []
	}
}

port_origin_and_destination = []

no_colon_error = "You have input addresses and port numbers in the wrong format \n Please enter address and port number in the form address:port"

def create_address_dictionary():
	for i in range(len(destinations)):
		#sequence_numbers.append(0)
		address_result = address_regex.findall(destinations[i])
		address = address_result[0] #retrieve the address from the return array from the regex
		port_result = port_regex.findall(destinations[i])

		ip_address_result = ip_address_regex.findall(address)

		if len(ip_address_result) == 0:
			ip_address = socket.gethostbyname(address)
		else:
			ip_address = address

		port_origin_and_destination.append((ip_address,port_result[0]))

		#dictionary of ip addresses for every adress provided
		address_translation[address] = ip_address


def send_packet(port_origin):
	address = port_origin[0]
	port = int(port_origin[1])
	json_message = json.dumps(traceroute)
	print("Sending packet to address and port number: %s %s"%(address,port) )
	clientSock.sendto(json_message.encode('utf-8'),(address,port))
	modifiedMessage, serverAddress = clientSock.recvfrom(2048)
	modifiedMessage = modifiedMessage.decode()
	print(modifiedMessage)
	received_json = json.loads(modifiedMessage)


create_address_dictionary()
port_origin = port_origin_and_destination[0]
port_destination = port_origin_and_destination[1]

port_origin_string = "%s:%s"%(port_origin[0],port_origin[1])
port_destination_string = "%s:%s"%(port_destination[0],port_destination[1])
print(port_origin_string)
print(port_destination_string)

clientSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
clientSock.settimeout(5)
clientSock.bind(("localhost",int(listening_port_number)))
print ("Listening on %s:%s"%("localhost",listening_port_number))

traceroute["Data"]["Destination"] = port_destination_string
traceroute["Data"]["Origin"] = port_origin_string

send_packet(port_origin)
