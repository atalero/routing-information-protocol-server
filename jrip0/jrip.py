#test command: python jrip.py -p 8005 -l 0 35.185.116.250:8001 35.185.116.250:8000
import sys, argparse, time, socket, re, json
from random import random
SLEEP_TIME = 2
address_translation = {}
ip_address_regex = re.compile(r'[0-9]{1,3}[.]{1}[0-9]{1,3}[.]{1}[0-9]{1,3}[.]{1}[0-9]{1,3}')
my_host_IP = socket.gethostbyname(socket.gethostname())

parser = argparse.ArgumentParser(description='Process the port number and loss.')
parser.add_argument('-p', metavar='port number', type=int, nargs=1, help='the receive port number', required=True)
parser.add_argument('-l', metavar='loss', type= float, nargs=1, help='loss probability, number between 0 and 1', required=True)
parser.add_argument('destinations', metavar='destinations', type=str, nargs='+', help='unlimited list of destination hosts and port numbers')

args = vars(parser.parse_args())
listening_port_number = args['p'][0] #port number specified in the p flag
loss_probability = args['l'][0]
destinations = args['destinations']
ips_and_port_numbers = tuple()
address_regex = re.compile(r'.+?(?=:)')
port_regex = re.compile(r'\:(.*)')


no_colon_error = "You have input addresses and port numbers in the wrong format \n Please enter address and port number in the form address:port"
def compute_whether_to_send():
	r = random()
	#print(r)
	if r <= loss_probability:
		return False
	return True

clientSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#clientSock.settimeout(0.001)
#clientSock.bind(("",listening_port_number))
#clientSock.bind((my_host_IP, listening_port_number))
#close()?
def send_packets():
	for i in range(len(destinations)):
		"""if not compute_whether_to_send():
			return
"""
		address_result = address_regex.findall(destinations[i])
		port_result = port_regex.findall(destinations[i])
		if len(port_result)== 0 or len(address_result) == 0:
			print (no_colon_error)
			sys.exit()

		address = address_translation[address_result[0]]
		port = int(port_result[0])

		print("Sending packet to address and port number: %s %s"%(address,port) )
		#clientSock.connect((address, port))

		clientSock.sendto("HI".encode('utf-8'),(address,port))
		print(clientSock.getsockname())
		#print("this client's socket number: %s"% clientSock.getsockname())
		modifiedMessage, serverAddress = clientSock.recvfrom(2048)
		print(modifiedMessage.decode())


def create_address_dictionary():
	for destination in destinations:
		address_result = address_regex.findall(destination)
		address = address_result[0] #retrieve the address from the return array from the regex

		ip_address_result = ip_address_regex.findall(address)

		if len(ip_address_result) == 0:
			ip_address = socket.gethostbyname(address)
		else:
			ip_address = address

		#dictionary of ip addresses for every adress provided
		address_translation[address] = ip_address

def client_side():
	try:
		while True:
			send_packets()
			time.sleep(SLEEP_TIME)
	except KeyboardInterrupt:
	    print('Manual break by user')

def main():
	create_address_dictionary()
	client_side()
	while 1:
	   pass

if __name__ == "__main__":
	main()
