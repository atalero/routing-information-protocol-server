#!/usr/bin/env python
"""This is the first version of jrip3"""
#test commands:
#python jrip3.py -p 8000 -l 0 localhost:8001:10 localhost:8002:10 & python jrip3.py -p 8001 -l 0.4 localhost:8000:10 & python jrip3.py -p 8002 -l 0.3 localhost:8000:10
#python jrip3.py -p 8000 -l 0 localhost:8001:10 & python jrip3.py -p 8001 -l 0 localhost:8000:10

#FIRST CONVERGENCE
#python jrip3.py -p 8001 -l 0 localhost:8004:15 localhost:8002:10
#python jrip3.py -p 8002 -l 0 localhost:8001:10 localhost:8003:5
#python jrip3.py -p 8003 -l 0 localhost:8002:5 localhost:8004:15
#python jrip3.py -p 8004 -l 0 localhost:8001:15 localhost:8003:15

#python jrip3.py -p 8001 -l 0 localhost:8004:15 localhost:8002:10 & python jrip3.py -p 8002 -l 0 localhost:8001:10 localhost:8003:5 & python jrip3.py -p 8003 -l 0 localhost:8002:5 localhost:8004:15 & python jrip3.py -p 8004 -l 0 localhost:8001:15 localhost:8003:15

#SECOND CONVERGENCE
#python jrip3.py -p 8001 -l 0 localhost:8004:15 localhost:8002:10 localhost:8003:3
#python jrip3.py -p 8002 -l 0 localhost:8001:10 localhost:8003:5
#python jrip3.py -p 8003 -l 0 localhost:8002:5 localhost:8004:15 localhost:8001:40
#python jrip3.py -p 8004 -l 0 localhost:8001:15 localhost:8003:15

import sys, argparse, time, socket, re, json, _thread, os, copy
from random import random
import queue

SLEEP_TIME = 1
WINDOW_SIZE = 5
TIMEOUT_VALUE = 1.5 #should be 0.5
MAX_PACKETS_RECEIVED = 100
MAX_PACKETS_SENT = 100

END_PROCESS_NUM_OF_SENDS = 110

address_translation = {}
goodput ={}
dictionary_classifier = dict()
neighbor_weight_vectors = dict() #record up to date vectors for all neighbors

receiver_queue = queue.Queue()

ip_address_regex = re.compile(r'[0-9]{1,3}[.]{1}[0-9]{1,3}[.]{1}[0-9]{1,3}[.]{1}[0-9]{1,3}')

parser = argparse.ArgumentParser(description='Process the port number and loss.')
parser.add_argument('-p', metavar='port number', type=int, nargs=1, help='the receive port number', required=True)
parser.add_argument('-l', metavar='loss', type= float, nargs=1, help='loss probability, number between 0 and 1', required=True)
parser.add_argument('destinations', metavar='destinations', type=str, nargs='+', help='unlimited list of destination hosts and port numbers')

args = vars(parser.parse_args())
listening_port_number = args['p'][0] #port number specified in the p flag
loss_probability = args['l'][0]
destinations = args['destinations']

ips_and_port_numbers = tuple()

address_regex = re.compile(r'.+?(?=:)')
port_regex = re.compile(r'\:(.*)')
link_weight_regex = re.compile(r'\:(.*)')

rip_table = {
	"Router": 0,
	"SEQ" : 1,
	"ACK" : 0,
	"Data" :
		{
			"Type" : "JRIP",
			"RIPTable": []
		}
	}

no_colon_error = "You have input addresses and port numbers in the wrong format \n Please enter address and port number in the form address:port"
def compute_whether_to_send():
	r = random()
	if r <= loss_probability:
		return True
	return False

def create_address_dictionary():
	global rip_table
	global sequence_numbers

	rip_table["Router"] = "127.0.0.1:%s"%listening_port_number

	for i in range(len(destinations)):
		#sequence_numbers.append(0)
		address_result = address_regex.findall(destinations[i])
		port_result = port_regex.findall(destinations[i])
		link_weight_result = link_weight_regex.findall(port_result[0])
		port_result = address_regex.findall(port_result[0])

		address = address_result[0] #retrieve the address from the return array from the regex
		port = port_result[0]
		link_weight = int(link_weight_result[0])

		#Add all the neighbors to the RIP table
		ip_address = socket.gethostbyname(address)
		rip_table["Data"]["RIPTable"].append({"Dest": '%s:%s'%(ip_address,port), "Next": '%s:%s'%(ip_address,port), "Cost": link_weight})
		#dictionary of ip addresses for every adress provided
		address_translation[address] = ip_address
		goodput[destinations[i]] = set()

def output_rip_table():
	file = open("convergence.txt" ,"a")
	file.write("This router's port number: %s\n"%listening_port_number)
	file.write("This router's jrip_table: %s\n"%rip_table["Data"]["RIPTable"])
	file.close()

def sender(address):
	#takes the port number as command line arguments
	address_result = address_regex.findall(address)
	port_result = port_regex.findall(address)
	link_weight_result = link_weight_regex.findall(port_result[0])
	port_result = address_regex.findall(port_result[0])

	serverName = address_translation[address_result[0]]
	serverPort = int(port_result[0])

	#create client socket
	clientSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	clientSocket.settimeout(0.001)

	start = time.time()

	#initializes window variables (upper and lower window bounds, position of next seq number)
	base=0
	nextSeqnum=0
	windowSize=5
	window = []
	#first = True
	total_packets_sent = 0
	while True:
		if(nextSeqnum<base+windowSize):
			#create packet(seqnum,data,checksum)
			#start = time.time()
			sndpkt = copy.deepcopy(rip_table)
			sndpkt["SEQ"] = nextSeqnum

			time.sleep(SLEEP_TIME)
			#send packet only if compute_whether_to_send decideds not to lose it
			total_packets_sent += 1
			if not compute_whether_to_send():
				if sndpkt["SEQ"] not in goodput[address]:
					goodput[address].add(sndpkt["SEQ"])

				json_reply = json.dumps(sndpkt)
				clientSocket.sendto(json_reply.encode('utf-8'),(serverName, serverPort))
				#print ("Sent data %s" % nextSeqnum)
				if total_packets_sent == MAX_PACKETS_SENT:
					calculate_goodput(address)

				if total_packets_sent == END_PROCESS_NUM_OF_SENDS:
					output_rip_table()

				if total_packets_sent == END_PROCESS_NUM_OF_SENDS + 50:
					os._exit(1)

			if base == nextSeqnum:
				lastackreceived = time.time()

			#increment variable nextSeqnum
			nextSeqnum = nextSeqnum + 1
			#append packet to window
			window.append(sndpkt)

			x = []
			for i in window:
				x.append(i["SEQ"])
			#print(x)
		#RECEIPT OF AN ACK
		try:
			packet,serverAddress = clientSocket.recvfrom(4096)

			received_json = json.loads(packet.decode())
			#print ("Received ack for %s"%received_json["ACK"])

			#slide window and reset timer
			while received_json["ACK"]>base:
				lastackreceived = time.time()
				del window[0]
				base = base + 1

		#TIMEOUT
		except:
			if(time.time()-lastackreceived>TIMEOUT_VALUE):
				lastackreceived = time.time()
				#print("TIMEOUT")
				for i in window:

					#print("resending packet %s "%i["SEQ"])
					json_reply = json.dumps(i)
					#if not compute_whether_to_send():
					clientSocket.sendto(json_reply.encode('utf-8'),(serverName, serverPort))
					if i["SEQ"] not in goodput[address]:
						goodput[address].add(i["SEQ"])


					total_packets_sent += 1
					if total_packets_sent == MAX_PACKETS_SENT:
						calculate_goodput(address)


	print ("connection closed")
	clientSocket.close()

def calculate_goodput(address):
	file = open("goodput.txt","a")
	file.write("Address at this router: %s\n"%listening_port_number)
	file.write("Address of this neighbor: %s\n"% address)
	file.write("%s packets sent\n"%MAX_PACKETS_SENT)
	file.write("Number of unique packets sent: %s\n"% len(goodput[address]))
	file.write("Goodput: %s\n"% (len(goodput[address])/MAX_PACKETS_SENT))
	file.close()

def listening_thread(serverSocket):
	while True:
		packet,clientAddress= serverSocket.recvfrom(4096)
		receiver_queue.put((packet,clientAddress))

def update_my_weight_vector(received_json):
	identifier = received_json["Router"]
	my_neighbors = set()
	#received_json is the rip table of a neighbor
	#rip_table is the rip table of this router
	cost_to_neighbor = -1

	file = open("%s_LOG.txt"%listening_port_number,"a")
	file.write("neighbor ID: %s\n"% identifier)
	file.write("RIP table at this router: %s\n"%rip_table["Data"]["RIPTable"])
	file.write("\n")
	file.write("\n")
	file.close()

	for neighbor in rip_table["Data"]["RIPTable"]:
		my_neighbors.add(neighbor["Dest"])

	for neighbor in rip_table["Data"]["RIPTable"]:
		if neighbor["Dest"] == identifier:
			cost_to_neighbor =  neighbor["Cost"]

	if cost_to_neighbor == -1:
		print("NOT FOUND")
		return

	for neighbor_neighbor in received_json["Data"]["RIPTable"]:
		if neighbor_neighbor["Dest"] not in my_neighbors and neighbor_neighbor["Dest"] != rip_table["Router"]:
			this_neighbor = copy.deepcopy(neighbor_neighbor)
			this_neighbor["Cost"] += cost_to_neighbor
			this_neighbor["Next"] = identifier
			rip_table["Data"]["RIPTable"].append(this_neighbor)

	for neighbor_neighbor in received_json["Data"]["RIPTable"]:
		for neighbor in rip_table["Data"]["RIPTable"]:
			if neighbor_neighbor["Dest"] == neighbor["Dest"]:
				if neighbor["Cost"] > neighbor_neighbor["Cost"] + cost_to_neighbor:
					neighbor["Cost"] = neighbor_neighbor["Cost"] + cost_to_neighbor
					neighbor["Next"] = identifier

def receiver_responses(serverSocket):
	reply_packet = {
		"SEQ" : -1,
		"ACK" : 0,
		}

	while True:
		packet_to_process = receiver_queue.get()
		received_json = json.loads(packet_to_process[0].decode())
		IP = packet_to_process[1][0]
		port = packet_to_process[1][1]

		identifier = '%s:%s'%(IP,port)
		update_my_weight_vector(received_json)

		#keep in mind that this identifier is NOT the listening port of the neighbors, it is the sending port

		neighbor_weight_vectors[identifier] = received_json
		#use Bellman-Ford equation to update this router's weight vector
		if identifier not in dictionary_classifier:
			dictionary_classifier[identifier] = 0

		#dictionary_classifier[identifier] corresponds to expected sequence number
		if received_json["SEQ"] == dictionary_classifier[identifier]:
			#create ACK (seqnum,checksum)
			reply_packet["ACK"] = dictionary_classifier[identifier]

			#print("sending ACK: %s"%dictionary_classifier[identifier])
			dictionary_classifier[identifier] += 1

			json_reply = json.dumps(reply_packet)
			serverSocket.sendto(json_reply.encode('utf-8'),(IP, port))
		else:
			#print ("Received out of order, seq num: %s"%received_json["SEQ"])
			reply_packet["ACK"] = dictionary_classifier[identifier]
			json_reply = json.dumps(reply_packet)
			serverSocket.sendto(json_reply.encode('utf-8'),(IP, port))

def main():

	serverName="localhost"
	serverPort= int(listening_port_number)
	serverSocket=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	serverSocket.bind((serverName,serverPort))

	#print ("Listening on %s:%s"%(serverName,serverPort))

	create_address_dictionary()
	#print(rip_table)

	for destination in destinations:
		_thread.start_new_thread(sender,(destination,))

	_thread.start_new_thread(listening_thread,(serverSocket, ))
	_thread.start_new_thread(receiver_responses,(serverSocket, ))
	while 1:
	   pass

if __name__ == "__main__":
	main()
